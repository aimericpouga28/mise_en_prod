def sonarAnalysis(){
    withSonarQubeEnv('SonarQube') {
        echo "Analysing the App..."
                sh 'mvn sonarqube'
    }
}


def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t fermat28/my-repo:pmep-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push fermat28/my-repo:pmep-2.0'
    echo "Ok..."
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
